<?php

namespace SENSIT\BlogSubtitle\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddSubtitleFieldToBlogTable extends Migration
{
    public function up()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            $table->string('sensit_blogsubtitle_subtitle')->nullable();
        });
    }
    public function down()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            if (Schema::hasColumn('rainlab_blog_posts', 'sensit_blogsubtitle_subtitle')) {
                $table->dropColumn('sensit_blogsubtitle_subtitle');
            }
        });
    }
}
