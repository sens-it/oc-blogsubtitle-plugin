<?php return [
    'plugin' => [
        'name' => 'Blog Subtitle',
        'description' => 'Blog Subtitle extends the RainLab.Blog plugin with an additional field to specify a subtitlte to display with a blog post.'
    ],
    'post' => [
        'subtitle' => 'Subtitle'
    ]
];
