<?php namespace SENSIT\BlogSubtitle;

use Backend;
use System\Classes\PluginBase;

/**
 * BlogSubtitle Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'RainLab.Blog'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'sensit.blogsubtitle::lang.plugin.name',
            'description' => 'sensit.blogsubtitle::lang.plugin.description',
            'author'      => 'SENS IT UG (haftungsbeschränkt)',
            'icon'        => 'icon-header'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \RainLab\Blog\Models\Post::extend(function ($model) {
            $model->addDynamicMethod('hasSubtitle', function () use ($model) {
                return true;
            });
            $model->addDynamicMethod('subtitle', function () use ($model) {
                return $model->sensit_blogsubtitle_subtitle;
            });
        });
        
        \Event::listen('backend.form.extendFields', function ($widget) {
            
            // Only for the blog post controller
            if (! $widget->getController() instanceof \RainLab\Blog\Controllers\Posts) {
                return;
            }
            
            // Only for the blog post model
            if (! $widget->model instanceof \RainLab\Blog\Models\Post) {
                return;
            }
            
            if ($widget->getConfig('include', true)) {
                $widget->addSecondaryTabFields([
                        'sensit_blogsubtitle_subtitle' => [
                                'label' => 'sensit.blogsubtitle::lang.post.subtitle',
                                'tab' => 'rainlab.blog::lang.post.tab_manage',
                                'type' => 'text',
                        ]
                ]);
            }
        });
    }
}
