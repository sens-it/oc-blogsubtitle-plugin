<?php return [
    'plugin' => [
        'name' => 'Blog Subtitle',
        'description' => 'Blog Subtitle erweitert das RainLab.Blog Plugin mit einem zusätzlichen Feld für einen Untertitel, das sich mit dem Post anzeigen lässt.'
    ],
    'post' => [
        'subtitle' => 'Untertitel'
    ]
];
