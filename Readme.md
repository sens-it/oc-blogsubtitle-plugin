# Blog Subtitle

Blog Subtitle extends the RainLab.Blog plugin with an additional field to specify a subtitlte to display with a blog post.

![](docs/screenshot.png)


When the plugin is installed a new field **Subtitle** will appear on the *Manage* tab of blog posts. The entered text is available as a property of the blog post when rendering it. This allows to specify an additional heading (a subheading) for the blog post.

The new field **Subtitle** can be empty for blog posts. When accessing the `subtitle` property it will give the entered value.

The field values are stored in a new column added to the `rainlab_blog_posts` called `subtitle`.
